# !/usr/bash

cd /media/matthieuriolo/Backup/deepl/data

# download data
mkdir -p data/openimages
cd data/openimages
aws s3 --no-sign-request sync s3://open-images-dataset/validation . && aws s3 --no-sign-request sync s^C//open-images-dataset/test .
mkdir -p ../imagenet
cd ../imagenet
wget https://storage.googleapis.com/kagglesdsdata/competitions/17433/824213/imagenet_object_localization_patched2019.tar.gz?GoogleAccessId=web-data@kaggle-161607.iam.gserviceaccount.com&Expires=1636055149&Signature=ZmocP5I6xR2eSciAYbj4VMJNIWFPdJS5zz7xITq8aITPzlOs1zd%2FpmaDquP0%2FaUT8GwLhAaethbKMCmFgieiKXjiOh4mruKX9s9HK2lK2%2BP616IjStQKLDZ99JTy48p6TRHeppHEWh%2BS0emHR%2F%2F1ntYsZhIGKoOMH71fWp3Nogp4%2BfCQvD3UaOXusuBy1Mjk79KZrRt7hQPtzqW3f7gTy%2BTZt4VJAitHIR6trg217kAr0Mk%2BnOLZcurzLNRoEWePJuLdG%2BdTKAnAbL1fXPN1qS0cXivM1d4mJilnRIFp89gDBjOo8jYrL80iWX%2BqvhLwgkYirmMxnANlXFLNSQyxzQ%3D%3D&response-content-disposition=attachment%3B+filename%3Dimagenet_object_localization_patched2019.tar.gz


# resize files
cd ../..
rm -rf train
rm -rf val
rm -rf  resized
mkdir resized

cd crawled/
find . -iname "*.jpeg" | xargs -I {} sh -c 'convert {} -resize 256x256\> -resize x256\< -resize 256x\< -type truecolor -set colorspace sRGB ../resized/`basename "{}"`'
find . -iname "*.jpg" | xargs -I {} sh -c 'convert {} -resize 256x256\> -resize x256\< -resize 256x\< -type truecolor -set colorspace sRGB ../resized/`basename "{}"`'
cd ../imagenet/
find . -iname "*.jpeg" | xargs -I {} sh -c 'convert {} -resize 256x256\> -resize x256\< -resize 256x\< -type truecolor -set colorspace sRGB ../resized/`basename "{}"`'
find . -iname "*.jpg" | xargs -I {} sh -c 'convert {} -resize 256x256\> -resize x256\< -resize 256x\< -type truecolor -set colorspace sRGB ../resized/`basename "{}"`'
cd ../open-images/
find . -iname "*.jpeg" | xargs -I {} sh -c 'convert {} -resize 256x256\> -resize x256\< -resize 256x\< -type truecolor -set colorspace sRGB ../resized/`basename "{}"`'
find . -iname "*.jpg" | xargs -I {} sh -c 'convert {} -resize 256x256\> -resize x256\< -resize 256x\< -type truecolor -set colorspace sRGB ../resized/`basename "{}"`'

cd ../

# create LMDB
find resized -iname "*.jpeg" > resized_dummy.txt
find resized -iname "*.jpg" >> resized_dummy.txt

cat resized_dummy.txt | shuf -n 20000 > val_resized_dummy.txt
grep -v -x -f val_resized_dummy.txt resized_dummy.txt > train_resized_dummy.txt

nl train_resized_dummy.txt > train_resized_numbered.txt
awk ' { t = $1; $1 = $2; $2 = t; print; } ' train_resized_numbered.txt > train_resized.txt
nl -v 1500000 val_resized_dummy.txt > val_resized_numbered.txt
awk ' { t = $1; $1 = $2; $2 = t; print; } ' val_resized_numbered.txt > val_resized.txt
# --resize_height=256 --resize_width=256
/media/matthieuriolo/Backup/deepl/caffe_private_pascal_modified/build/tools/convert_imageset --shuffle --backend=lmdb  /media/matthieuriolo/Backup/deepl/data/ /media/matthieuriolo/Backup/deepl/data/train_resized.txt ./train
/media/matthieuriolo/Backup/deepl/caffe_private_pascal_modified/build/tools/convert_imageset --shuffle --backend=lmdb  /media/matthieuriolo/Backup/deepl/data/ /media/matthieuriolo/Backup/deepl/data/val_resized.txt ./val
