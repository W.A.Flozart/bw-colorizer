# how to compile caffe

https://shengshuyang.github.io/A-step-by-step-guide-to-Caffe.html

http://caffe.berkeleyvision.org/installation.html#compilation


http://caffe.berkeleyvision.org/tutorial/layers/imagedata.html
http://caffe.berkeleyvision.org/tutorial/layers/data.html

sudo apt install g++-9
sudo apt install nvidia-cuda-toolkit libatlas3-base libboost-all-dev
sudo apt install protobuf glog gflags hdf5
sudo apt install libgflags-dev
sudo apt install libhdf5-dev
sudo apt install libgoogle-glog-dev
sudo apt install libprotobuf-dev
sudo apt install protobuf-compiler
sudo apt install libopenblas-base
sudo apt install libatlas-base-dev
sudo apt install libopencv3-dev
sudo apt install libopencv-core-dev
sudo apt install libhdf5-dev
sudo apt install python3-dev
sudo apt install gcc-9
sudo apt install nvidia-utils-470
sudo apt install liblmdb-dev

for req in $(cat python/requirements.txt); do pip3 install $req; done
sudo apt install python3-numpy


# nach compilation

sudo apt install nvidia-utils-470
sudo pip3 install numpy
sudo pip3 install sklearn

# path for the python layer
export PYTHONPATH=/media/matthieuriolo/Backup/deepl/caffe_private_pascal_modified/python:/media/matthieuriolo/Backup/deepl/colorization/resources/:$PYTHONPATH

