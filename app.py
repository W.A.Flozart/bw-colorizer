import os, fnmatch
import glob
from flask import Flask, request, redirect, url_for, render_template, send_from_directory,flash 
from werkzeug.utils import secure_filename
import cv2
import numpy as np

app = Flask(__name__, static_url_path="/static")
UPLOAD_FOLDER = 'static/uploads/'
GREY_IMAGES_FOLDER = 'static/grey_images/'
PROCESSED_IMAGES_FOLDER = 'static/processed_images/'
ALLOWED_EXTENSIONS = {'jpg', 'png','jpeg'}


# APP CONFIGURATIONS
app.config['SECRET_KEY'] = 'opencv'  
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['GREY_IMAGES_FOLDER'] = GREY_IMAGES_FOLDER
app.config['PROCESSED_IMAGES_FOLDER'] = PROCESSED_IMAGES_FOLDER
# limit upload size upto 2mb
app.config['MAX_CONTENT_LENGTH'] = 2 * 1024 * 1024

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file attached in request')
            return redirect(request.url)
        file = request.files['file']
        if file.filename == '':
            flash('No file selected')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            process_file(os.path.join(UPLOAD_FOLDER, filename), filename)
            data={
                "processed_img_v1":'static/processed_images/' + "colornet_iter_32731_" + filename,
                "processed_img_v2":'static/processed_images/' + "colornet_iter_500000_" + filename,
                "processed_img_v3":'static/processed_images/' + "colornet_iter_500000_v6_" + filename,
                "processed_img_v4":'static/processed_images/' + "colornet_iter_533000_v7_" + filename,
                "processed_img_v5":'static/processed_images/' + "colorization_release_v2_zhang_" + filename,
                "grey_image":'static/grey_images/' + "grey_" + filename,
                "uploaded_img":'static/uploads/'+filename
            }
            return render_template("index.html",data=data)  
    return render_template('index.html')


def process_file(path, filename):
    colorize_image(path, filename)
    

def colorize_image(path, filename):

    models = ["colorization_release_v2_zhang", "colornet_iter_32731", "colornet_iter_500000", "colornet_iter_500000_v6", "colornet_iter_533000_v7"]

    image = cv2.imread(path)

    img_width = image.shape[1]
    scaling_ratio = 500 / img_width

    width = 500
    height = int(image.shape[0] * scaling_ratio)
    dsize = (width, height)

    image = cv2.resize(image, dsize)


    image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)
    cv2.imwrite(f"{GREY_IMAGES_FOLDER}grey_{filename}", cv2.cvtColor(image, cv2.COLOR_RGB2BGR))


    for model_variant in models:
        # Define model paths
        prototxt = "./model/colorization_deploy_v2.prototxt"
        model = "./model/" + model_variant + ".caffemodel"
        points = "./model/pts_in_hull.npy"
        # Load serialized black and white colorizer model and cluster
        net = cv2.dnn.readNetFromCaffe(prototxt, model)
        pts = np.load(points)

        # Add the cluster centers as 1x1 convolutions to the model
        class8 = net.getLayerId("class8_ab")
        conv8 = net.getLayerId("conv8_313_rh")
        pts = pts.transpose().reshape(2, 313, 1, 1)
        net.getLayer(class8).blobs = [pts.astype("float32")]
        net.getLayer(conv8).blobs = [np.full([1, 313], 2.606, dtype="float32")]

        # Load the input image, scale it and convert it to Lab
        image = cv2.imread(path)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)


        # Normalize image
        scaled = image.astype("float32") / 255.0
        # Cobert to LAB
        lab = cv2.cvtColor(scaled, cv2.COLOR_RGB2LAB)
        # Resize image
        resized = cv2.resize(lab, (224, 224))
        # Extract "L" from normalized and resized input image
        L = cv2.split(resized)[0]
        L -= 50

        # Set L as input
        net.setInput(cv2.dnn.blobFromImage(L))
        # Predict "a" and "b" for input image
        ab = net.forward()[0, :, :, :].transpose((1, 2, 0))
        # Resize ab to shape of original input image
        ab = cv2.resize(ab, (image.shape[1], image.shape[0]))



        # Create a colorized Lab image (L + a + b)
        # Take L from original input
        L = cv2.split(lab)[0]
        # Combine L,a and b layers
        colorized = np.concatenate((L[:, :, np.newaxis], ab), axis=2)

        # Convert colorized Lab image to RGB
        colorized = cv2.cvtColor(colorized, cv2.COLOR_LAB2RGB)
        # Set pixel < 0 to 0 and pixels > 1 to 1
        colorized = np.clip(colorized, 0, 1)
        # Restore normalized values to values 0 - 255
        colorized = (255 * colorized).astype("uint8")


        print(f"{PROCESSED_IMAGES_FOLDER}{model_variant}_{filename}")

        # Save final RGB photo
        cv2.imwrite(f"{PROCESSED_IMAGES_FOLDER}{model_variant}_{filename}", cv2.cvtColor(colorized, cv2.COLOR_RGB2BGR))

# Gallery
@app.route('/gallery/')
def gallery():

    # get image paths 
    included_extensions = ['jpg','jpeg', 'png']

    path_images_original = os.path.join(app.static_folder, "uploads")
    images_original = [fn for fn in os.listdir(path_images_original)
              if any(fn.endswith(ext) for ext in included_extensions)]

    path_images_processed = os.path.join(app.static_folder, "processed_images")
    images_processed = [fn for fn in os.listdir(path_images_processed)
              if any(fn.endswith(ext) for ext in included_extensions)]

    return render_template('gallery.html', images_original = images_original, images_processed = images_processed)

# download 
# @app.route('/uploads/<filename>')
# def uploaded_file(filename):
#    return send_from_directory(app.config['PROCESSED_IMAGES_FOLDER'], filename, as_attachment=True)

if __name__ == '__main__':
    app.run()
