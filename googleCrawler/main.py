import os
import threading
import time
import ssl
import urllib.request
from datetime import datetime

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys

DOWNLOAD_DIR = 'D:\\FFHS\\DeepL\\googleCrawler'
prefs = {"download.default_directory": DOWNLOAD_DIR}
options = webdriver.ChromeOptions()
options.add_experimental_option("prefs", prefs)


def crawl_n_images_using_term(n=10, term='Dog'):
    driver = webdriver.Chrome(options=options, executable_path='./chromedriver.exe')
    driver.maximize_window()
    driver.get('https://www.google.ch/imghp?hl=en&tab=ri&authuser=0&ogbl')

    # accept cookies
    driver.find_element(By.XPATH, '//*[@id="L2AGLb"]').click()

    # search bar
    search_bar = driver.find_element(By.XPATH, '//*[@id="sbtc"]/div/div[2]/input')
    search_bar.send_keys(term)
    search_bar.send_keys(Keys.ENTER)

    '''# set to large images only
    # tools
    driver.find_element(By.XPATH, '//*[@id="yDmH0d"]/div[2]/c-wiz/div[1]/div/div[1]/div[2]/div/div/div').click()
    # size
    time.sleep(0.3)
    driver.find_element(By.XPATH,
                        '//*[@id="yDmH0d"]/div[2]/c-wiz/div[2]/div[2]/c-wiz[1]/div/div/div[1]/div/div[1]/div').click()
    # large
    time.sleep(0.3)
    driver.find_element(By.XPATH,
                        '//*[@id="yDmH0d"]/div[2]/c-wiz/div[2]/div[2]/c-wiz[1]/div/div/div[3]/div/a[2]').click()'''

    # scroll to load everything
    last_height = driver.execute_script('return document.body.scrollHeight')
    while True:
        driver.execute_script('window.scrollTo(0,document.body.scrollHeight)')
        time.sleep(2)
        new_height = driver.execute_script('return document.body.scrollHeight')
        try:
            driver.find_element(By.XPATH, '//*[@id="islmp"]/div/div/div/div/div[5]/input').click()
            time.sleep(2)
        except:
            pass
        if new_height == last_height:
            break
        last_height = new_height

    pic_urls = []
    for i in range(1, n + 1):
        try:
            # img preview
            driver.find_element(By.XPATH, f'//*[@id="islrg"]/div[1]/div[{i}]/a[1]/div[1]/img').click()

            # img preview right
            img = driver.find_element(By.XPATH,
                                      '//*[@id="Sva75c"]/div/div/div[3]/div[2]/c-wiz/div/div[1]/div[1]/div[2]/div/a/img')
            # time.sleep(0.1)
            # get img src link
            pic_urls.append(img.get_attribute('src'))
        except:
            pass

    # crawl all img links
    c = 0
    for link in pic_urls:
        ssl._create_default_https_context = ssl._create_unverified_context
        try:
            urllib.request.urlretrieve(link, f"data/{term}_{c}.png")
            URLS.append(link)
            print(f'Saved {c} image')
        except:
            pass
        c += 1

    driver.close()


def load_words():
    '''with open('words_alpha.txt') as word_file:
        valid_words_en = set(word_file.read().split('\n'))
        return list(valid_words_en)'''
    with open('words_en.txt') as word_file:
        valid_words_en = set(word_file.read().split('\n'))
    with open('words_es.txt') as word_file:
        valid_words_es = set(word_file.read().split('\n'))
    with open('words_ru.txt', encoding='utf-8', mode='r') as word_file:
        valid_words_ru = set(word_file.read().split('\n'))

    return list(set.union(valid_words_en, valid_words_es, valid_words_ru))


# Amount of pictures per term
N = 100
TERMS = load_words()
URLS = []
# time script
startTime = datetime.now()
print('[+] Started Crawl...')
#for t in TERMS:
    #crawl_n_images_using_term(20, t)
maxThreads = 4 # os.cpu_count()
chunksize = maxThreads
n=0

while chunksize >= maxThreads:
    chunk = TERMS[n:n + maxThreads]
    chunksize = len(chunk)
    threads = list()
    for t in chunk:
        thread = threading.Thread(target=crawl_n_images_using_term, args=(N, t))
        threads.append(thread)
        thread.start()
        # crawl_n_images_using_term(20, t)
        print(f'Finished Term: {t}')

    for index, thread in enumerate(threads):
        thread.join()

    n += maxThreads

print('[+] Finished Crawl')
print('[+] Took:', datetime.now() - startTime)

with open('urls.txt', 'w') as f:
    for url in URLS:
        f.write("%s\n" % url)
