from PIL import Image
import numpy as np


def get_pixel_colors(url):
    im = Image.open(url, 'r')
    im = im.convert("RGBA")
    pixel_values = list(im.getdata())
    return np.array(pixel_values)


original = 'original.jpg'
colorized = 'colorized.jpg'

original_pixels = get_pixel_colors(original)
colorized_pixels = get_pixel_colors(colorized)

r_diff = 0
g_diff = 0
b_diff = 0
for i in range(0, len(original_pixels)):
    r_diff += abs(original_pixels[i][0] - colorized_pixels[i][0])
    g_diff += abs(original_pixels[i][1] - colorized_pixels[i][1])
    b_diff += abs(original_pixels[i][2] - colorized_pixels[i][2])

print("Total Difference:")
print("Red: "+str(r_diff))
print("Green: "+str(g_diff))
print("Blue: "+str(b_diff))
print("----------")
print("Avg. Difference:")
print("Red: "+str(r_diff/len(original_pixels)))
print("Green: "+str(g_diff/len(original_pixels)))
print("Blue: "+str(b_diff/len(original_pixels)))
print("----------")
print("Total Pixels: "+str(len(original_pixels)))